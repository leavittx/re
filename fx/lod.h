#pragma once

#include "../globals.h"

class LodScene : public redemo::Scene, recore::KeyboardEventsListener
{
public:
	LodScene() {}
	~LodScene() {}

	void init();
	void update();
	void draw();
	void release();

	virtual void handleKeyboardEvent(recore::Key key);

private:
	void makePlane(render::GLBatch& planeBatch, int width, int height);
	void makePlane(render::GLMyBatch& batch, int width, int height);
};
