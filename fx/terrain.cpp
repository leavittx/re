#include "terrain.h"
#include "core/time.h"
//#include "render/glhelper.h"
#include "render/texturemanager.h"
#include "render/shadermanager.h"
#include "util/imagefactory.h"
#include "util/tga.h"
#include <math.h>

using namespace redemo;
using namespace recore;
using namespace render;
using namespace remath;
using namespace reutil;
using namespace std;

#define CAMERA_LINEAR 0.1f
#define BASE_MESH_SIZE 256
#define TERRAIN_SIZE 1.0f

//#define HEIGHT_MAP "terrain.bmp"
//#define HEIGHT_MAP "diff_2048.png"
#define HEIGHT_MAP "o1_sq.png"
//#define HEIGHT_MAP "o.jpg"
//#define HEIGHT_MAP "huy.tga"
//#define HEIGHT_MAP "luch.jpg"
//#define HEIGHT_MAP "color_tile_256.png"
//#define HEIGHT_MAP "bmtlogo_sq.png"
//#define HEIGHT_MAP "M.BMP"
//#define HEIGHT_MAP "grand_canyon_height_512.tga"
//#define HEIGHT_MAP "terrain-heightmap.tga"


float TerrainScene::getLightness(unsigned int color) const
{
	return 1.0f / color;
}

float TerrainScene::getLightness(unsigned int r, unsigned int g, unsigned int b) const
{
	return 1.0f / (max_(max_(r, g), b) + min_(min_(r, g), b));
}

float TerrainScene::getHeight(Image* heightMap, int x, int y)
{
	int pixel = heightMap->getData()[x*m_size + y];

	switch (heightMap->getFormat())
	{
	case GL_LUMINANCE:
		return getLightness(pixel);

	case GL_BGR:
	{
		int idx = (m_size-x-1) * m_size + y;
		return getLightness(heightMap->getData()[idx*3 + 0],
							heightMap->getData()[idx*3 + 1],
							heightMap->getData()[idx*3 + 2]);
		return 0;
	}

//	case GL_DEPTH_COMPONENT:
//		return getLightness(pixel);

	default:
		g_debug << "ERROR: unknown format of heightmap" << endl;
		return 0;
	}
}


bool TerrainScene::loadHeightMap(const string& path, Color4 vertexColor, int smoothFactor)
{
	m_heightmapFile = path;

	if (m_heightmapFile.empty())
		return false;

	Image* heightMap = ImageFactory::loadAny(m_heightmapFile);

	if (!heightMap)
	{
		g_debug << "ERROR: unable to load heightmap" << endl;
		return false;
	}

	// Get the dimension of the heightmap data
	m_size = heightMap->getHeight();

	int width = BASE_MESH_SIZE, height = BASE_MESH_SIZE;

	float *pos = new float[width*height*3];

	float s_factor = TERRAIN_SIZE/float(width-1);
	float t_factor = TERRAIN_SIZE/float(height-1);
	for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++) {
			int offset = (y*width+x)*3;
			float s = float(x) * s_factor;
			float t = float(y) * t_factor;
//		if terrain:
			int x_tex = int(/*round*/floorf(s * float(m_size-1)));
			int y_tex = int(/*round*/floorf(t * float(m_size-1)));
			float h = getHeight(heightMap, x_tex, y_tex);

//			h = terrain[x_tex, y_tex][3]
//		else:
//			h = 0

			pos[offset + 0] = s;
			pos[offset + 1] = t;
			pos[offset + 2] = h;
//			pos[offset + 3] = h; //jewish stars!
		}

	float *normals = new float[width*height*3];

//	if terrain:
	for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++) {
			int offset = (y*width+x) * 3;
			float s_ = float(x)*s_factor;
			float t_ = float(y)*t_factor;

			int x_tex = int(floorf(s_ * float(m_size-1)));
			int y_tex = int(floorf(t_ * float(m_size-1)));
			int x_shift = int(floor(s_factor * float(m_size-1)));
			int y_shift = int(floor(t_factor * float(m_size-1)));

			Vector3f up = Vector3f(0.0, 0.0, 1.0);
			//			float h = terrain[x_tex,y_tex][3]

			float h = getHeight(heightMap, x_tex, y_tex);

			Vector3f l = Vector3f(-s_factor, 0.0, getHeight(heightMap, x_tex-x_shift, y_tex) - h);
			Vector3f r = Vector3f(+s_factor, 0.0, getHeight(heightMap, x_tex+x_shift, y_tex) - h);
			Vector3f t = Vector3f(0.0, -t_factor, getHeight(heightMap, x_tex, y_tex-y_shift) - h);
			Vector3f b = Vector3f(0.0, +t_factor, getHeight(heightMap, x_tex, y_tex+y_shift) - h);

			Vector3f normal = ( l.crossProduct(Vector3f(0.0, -1.0, 0.0)).normalize() +
								r.crossProduct(Vector3f(0.0, 1.0, 0.0)).normalize() +
								t.crossProduct(Vector3f(1.0, 0.0, 0.0)).normalize() +
								b.crossProduct(Vector3f(-1.0, 0.0, 0.0)).normalize()
							  ).normalize();

			normals[offset + 0] = normal.x;
			normals[offset + 1] = normal.y;
			normals[offset + 2] = normal.z;
		}

	int i_width = width-1, i_height = height-1;
	unsigned short *ind = new unsigned short[i_width*i_height*6];
	for (int y = 0; y < i_height; y++)
		for (int x = 0; x < i_width; x++) {
			int offset = (x+y*i_width)*6;
			int p1 = x+y*width;
			int p2 = p1+width;
			int p4 = p1+1;
			int p3 = p2+1;
			ind[offset + 0] = p1;
			ind[offset + 1] = p2;
			ind[offset + 2] = p3;
			ind[offset + 3] = p1;
			ind[offset + 4] = p3;
			ind[offset + 5] = p4;
		}

	m_terrainBatch.Begin(GL_PATCHES, i_width*i_height*6, width*height*3);
	m_terrainBatch.CopyVertexData3f(pos);
	m_terrainBatch.CopyNormalDataf(normals);
	m_terrainBatch.CopyIndexData(ind);
	m_terrainBatch.End();

	delete [] pos;
	delete [] normals;
	delete [] ind;

	return true;
}


void TerrainScene::init()
{
	loadHeightMap("data/graphics/" + string(HEIGHT_MAP));
//	loadHeightMap("data/graphics/terrain-heightmap.tga");

	m_projectionMatrix.LoadMatrix(Matrix4f(gl::m_viewFrustum.GetProjectionMatrix()).ptr());
	m_transformPipeline.SetMatrixStacks(m_modelViewMatrix, m_projectionMatrix);

	InputManager::inst().acceptKeyboardEvents(this);
}

void TerrainScene::release()
{
}

void TerrainScene::update()
{
}

void TerrainScene::draw()
{
	gl::clear(gl::ALL);

	// Save the current modelview matrix (the identity matrix)
	m_modelViewMatrix.PushMatrix();

	M3DMatrix44f mCamera;
	m_cameraFrame.GetCameraMatrix(mCamera);
	m_modelViewMatrix.PushMatrix(mCamera);

	m_modelViewMatrix.Translate(0.0f, 0.0f, -2.5f);

	Matrix4f mTranslate, mRotate, mModelview, mModelViewProjection;
	mTranslate = Matrix4f::Translation(0.0f, 0.0f, -2.5f);
	mRotate = Matrix4f::RotationWithAxis(Vector3f(1.0f, 1.0f, -0.5f), remath::deg2rad(30*sin(t::gets() * 10.0f)));
	mModelview = mTranslate * mRotate;
	mModelViewProjection = Matrix4f(gl::m_viewFrustum.GetProjectionMatrix()) * mModelview;


	Shader& terrain = ShaderManager::inst().getShader("terrain");
	terrain.bind();
	terrain.setUniformMatrix4fv("mvp", 1, false, Matrix4f(m_transformPipeline.GetModelViewProjectionMatrix()).ptr());
	terrain.setUniformMatrix4fv("modelview", 1, false, Matrix4f(m_transformPipeline.GetModelViewMatrix()).ptr());
	terrain.setUniformMatrix4fv("projection", 1, false, Matrix4f(m_transformPipeline.GetProjectionMatrix()).ptr());
	terrain.setUniform2fv("screen_size", 1, gl::getResolution());

//	TextureManager::inst().bindTexture("grand_canyon_height_512.tga");
//	TextureManager::inst().bindTexture("M.BMP");
//	TextureManager::inst().bindTexture("terrain-heightmap.tga");
	TextureManager::inst().bindTexture(HEIGHT_MAP);

	terrain.setUniform1i("terrain", 0);

//	glPatchParameteri(GL_PATCH_VERTICES, 4);
	m_terrainBatch.Draw();


	// Restore the previous modleview matrix (the identity matrix)
	m_modelViewMatrix.PopMatrix();
	m_modelViewMatrix.PopMatrix();
}

void TerrainScene::handleKeyboardEvent(Key key)
{
	float linear = CAMERA_LINEAR;
	float angular = deg2rad(10.0f);

	switch (key) {
	case KeyTab:
	{
		m_wireframe = !m_wireframe;
		if (m_wireframe)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	}
	case KeyCapsLock:
	{
		m_points = !m_points;
		if (m_points)
			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	}
	case KeyUp:			m_cameraFrame.MoveForward(linear); break;
	case KeyDown:		m_cameraFrame.MoveForward(-linear); break;
	case KeyRight:		m_cameraFrame.MoveRight(-linear); break;
	case KeyLeft:		m_cameraFrame.MoveRight(linear); break;
	case KeyPageUp:		m_cameraFrame.MoveUp(linear); break;
	case KeyPageDown:	m_cameraFrame.MoveUp(-linear); break;
	case KeyW:			m_cameraFrame.RotateWorld( angular, 0.0f, 1.0f, 0.0f); break;
	case KeyS:			m_cameraFrame.RotateWorld(-angular, 0.0f, 1.0f, 0.0f); break;
	case KeyA:			m_cameraFrame.RotateWorld( angular, 0.0f, 0.0f, 1.0f); break;
	case KeyD:			m_cameraFrame.RotateWorld(-angular, 0.0f, 0.0f, 1.0f); break;
	case KeyQ:			m_cameraFrame.RotateWorld( angular, 1.0f, 0.0f, 0.0f); break;
	case KeyE:			m_cameraFrame.RotateWorld(-angular, 1.0f, 0.0f, 0.0f); break;
	default: return;
	}
}
