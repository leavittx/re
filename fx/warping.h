#pragma once

#include "../globals.h"
#include "render/glhelper.h"

class WarpingScene : public redemo::Scene, recore::KeyboardEventsListener
{
public:
	WarpingScene() {}
	~WarpingScene() {}

	void init();
	void update();
	void draw();
	void release();

	virtual void handleKeyboardEvent(recore::Key key);

private:
	render::GLBatch m_fullscreenQuad;
};
