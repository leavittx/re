#include "lod.h"
#include "core/time.h"
#include "render/texturemanager.h"
#include "render/shadermanager.h"
#include "util/tga.h"

//#include <thread>
#include <unistd.h>

using namespace redemo;
using namespace recore;
using namespace render;
using namespace remath;

static GLBatch triangleBatch;
static GLBatch cubeBatch;
static GLMyBatch planeBatch;

static GLFrame m_cameraFrame;
static GLMatrixStack m_modelViewMatrix;
static GLMatrixStack m_projectionMatrix;
static GLGeometryTransform m_transformPipeline;

static bool m_wireframe, m_points;


void LodScene::makePlane(GLBatch& planeBatch, int width, int height)
{
	planeBatch.Begin(GL_PATCHES, 4, 1);

	planeBatch.Vertex3f(width, -height, 0.0f);
	planeBatch.Vertex3f(width, height, 0.0f);
	planeBatch.Vertex3f(-width, height, 0.0f);
	planeBatch.Vertex3f(-width, -height, 0.0f);

//	planeBatch.Vertex3f(-width, height, 0.0f);
//	planeBatch.Vertex3f(-width, -height, 0.0f);
//	planeBatch.Vertex3f(width, -height, 0.0f);

	planeBatch.End();
}

void LodScene::makePlane(GLMyBatch& batch, int width, int height)
{
	float *pos = new float[width * height * 3];
	float width_factor = 30.0/float(width), height_factor = 30.0/float(height);
	for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++) {
			int offset = (x+y*width)*3;
			pos[offset + 0] = x*width_factor;
			pos[offset + 1] = y*height_factor;
			pos[offset + 2] = -3.0f;
		}

	int i_width = width-1, i_height = height-1;
	unsigned short *ind = new unsigned short[i_width * i_height * 4];
	for (int y = 0; y < i_height; y++)
		for (int x = 0; x < i_width; x++) {
			int offset = (x+y*i_width)*4;
			int p1 = x+y*width;
			int p2 = p1+width;
			int p4 = p1+1;
			int p3 = p2+1;
			ind[offset + 0] = p1;
			ind[offset + 1] = p2;
			ind[offset + 2] = p3;
			ind[offset + 3] = p4;
		}


	batch.Begin(GL_PATCHES, i_width * i_height * 4, width * height * 3);
	batch.CopyVertexData3f(pos);
	batch.CopyIndexData(ind);
	batch.End();

	delete [] pos;
	delete [] ind;
}


void LodScene::init()
{
	// Load up a triangle
	GLfloat vVerts[] = { -0.5f, 0.0f, 0.0f,
						  0.5f, 0.0f, 0.0f,
						  0.0f, 0.5f, 0.0f };

	triangleBatch.Begin(GL_PATCHES, 3);
	triangleBatch.CopyVertexData3f(vVerts);
	triangleBatch.End();

	MakeCube(cubeBatch, 1.0f);

	makePlane(planeBatch, 32, 32);

	m_projectionMatrix.LoadMatrix(Matrix4f(gl::m_viewFrustum.GetProjectionMatrix()).ptr());
	m_transformPipeline.SetMatrixStacks(m_modelViewMatrix, m_projectionMatrix);

	InputManager::inst().acceptKeyboardEvents(this);
}

void LodScene::release()
{
}

void LodScene::update()
{
//	usleep(16000);
}

void LodScene::draw()
{
	gl::clear(gl::ALL);

	// Save the current modelview matrix (the identity matrix)
	m_modelViewMatrix.PushMatrix();

	M3DMatrix44f mCamera;
	m_cameraFrame.GetCameraMatrix(mCamera);
	m_modelViewMatrix.PushMatrix(mCamera);

	m_modelViewMatrix.Translate(0.0f, 0.0f, -2.5f);

	Matrix4f mTranslate, mRotate, mModelview, mModelViewProjection;
	mTranslate = Matrix4f::Translation(0.0f, 0.0f, -2.5f);
	mRotate = Matrix4f::RotationWithAxis(Vector3f(1.0f, 1.0f, -0.5f), remath::deg2rad(30*sin(t::gets() * 10.0f)));
	mModelview = mTranslate * mRotate;
	mModelViewProjection = Matrix4f(gl::m_viewFrustum.GetProjectionMatrix()) * mModelview;


	Shader& lod = ShaderManager::inst().getShader("lod");
	lod.bind();
	lod.setUniform1f("pixels_per_division", 15.0f);
	lod.setUniform1i("projected", false);
	lod.setUniformMatrix4fv("modelview", 1, false, Matrix4f(m_transformPipeline.GetModelViewMatrix()).ptr());
							//mModelview.ptr());
	lod.setUniformMatrix4fv("projection", 1, false, Matrix4f(m_transformPipeline.GetProjectionMatrix()).ptr());
							//Matrix4f(gl::m_viewFrustum.GetProjectionMatrix()).ptr());
	lod.setUniform2fv("screen_size", 1, gl::getResolution());

	glPatchParameteri(GL_PATCH_VERTICES, 4);

//	gl::debug("888");

//	triangleBatch.Draw();
//	cubeBatch.Draw();
	planeBatch.Draw();

//	gl::debug("999");

	// Restore the previous modleview matrix (the identity matrix)
	m_modelViewMatrix.PopMatrix();
	m_modelViewMatrix.PopMatrix();
}

void LodScene::handleKeyboardEvent(Key key)
{
	float linear = 0.3f;
	float angular = deg2rad(10.0f);

	switch (key) {
	case KeyTab:
	{
		m_wireframe = !m_wireframe;
		if (m_wireframe)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	}
	case KeyCapsLock:
	{
		m_points = !m_points;
		if (m_points)
			glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	}
	case KeyUp:			m_cameraFrame.MoveForward(linear); break;
	case KeyDown:		m_cameraFrame.MoveForward(-linear); break;
	case KeyRight:		m_cameraFrame.MoveRight(-linear); break;
	case KeyLeft:		m_cameraFrame.MoveRight(linear); break;
	case KeyPageUp:		m_cameraFrame.MoveUp(linear); break;
	case KeyPageDown:	m_cameraFrame.MoveUp(-linear); break;
	case KeyW:			m_cameraFrame.RotateWorld( angular, 0.0f, 1.0f, 0.0f); break;
	case KeyS:			m_cameraFrame.RotateWorld(-angular, 0.0f, 1.0f, 0.0f); break;
	case KeyA:			m_cameraFrame.RotateWorld( angular, 0.0f, 0.0f, 1.0f); break;
	case KeyD:			m_cameraFrame.RotateWorld(-angular, 0.0f, 0.0f, 1.0f); break;
	case KeyQ:			m_cameraFrame.RotateWorld( angular, 1.0f, 0.0f, 0.0f); break;
	case KeyE:			m_cameraFrame.RotateWorld(-angular, 1.0f, 0.0f, 0.0f); break;
	default: return;
	}
}
