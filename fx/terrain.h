#pragma once

#include "../globals.h"

#include "render/glhelper.h"

class TerrainScene : public redemo::Scene, recore::KeyboardEventsListener
{
public:
	TerrainScene() {}
	~TerrainScene() {}

	void init();
	void update();
	void draw();
	void release();

	virtual void handleKeyboardEvent(recore::Key key);

private:
	render::GLFrame m_cameraFrame;
	render::GLMatrixStack m_modelViewMatrix;
	render::GLMatrixStack m_projectionMatrix;
	render::GLGeometryTransform m_transformPipeline;

	bool m_wireframe, m_points;

	std::string m_heightmapFile;
	int	m_size;
	render::GLMyBatch m_terrainBatch;

	bool loadHeightMap(const std::string& path, remath::Color4 vertexColor = remath::Color4(1.0f, 1.0f, 1.0f, 1.0f), int smoothFactor = 0);

	float getLightness(unsigned int color) const;
	float getLightness(unsigned int r, unsigned int g, unsigned int b) const;
	float getHeight(reutil::Image* heightMap, int x, int y);
};
