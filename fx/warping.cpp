#include "warping.h"
#include "core/time.h"
#include "render/texturemanager.h"
#include "render/shadermanager.h"

using namespace recore;
using namespace remath;
using namespace render;

void WarpingScene::init()
{
	MakeFullscreenQuad(m_fullscreenQuad);
}

void WarpingScene::release()
{
}

void WarpingScene::update()
{
}

void WarpingScene::draw()
{
//	static Color4 col(0.3f, 0.3f, 0.3f, 1.0f);

	gl::clear(gl::ALL);

//	ShaderManager::inst().UseStockShader(GLT_SHADER_IDENTITY, StockShaderUniforms(&col.r, true));

//	Shader& fx_monjori = ShaderManager::inst().getShader("fx_tunnel");
//	fx_monjori.bind();
//	fx_monjori.setUniform1f("time", float(t::get()));
//	fx_monjori.setUniform2fv("resolution", 1, gl::getResolution());
//	TextureManager::inst().bindTexture("rblur000.png", GL_TEXTURE0_ARB);
//	fx_monjori.setUniform1i("tex0", 0);
//	static float wave[3] = { 1.0f, 1.0f, 1.0f };
//	fx_monjori.setUniform3fv("wave", 1,	wave);

//#define STRCAST(a) ((char *)a)
//	glDrawElements();

	Shader& fx_wraping = ShaderManager::inst().getShader("fx_warping");
	fx_wraping.bind();
	fx_wraping.setUniform1f("time", float(t::get()));
	fx_wraping.setUniform2fv("resolution", 1, gl::getResolution());
	TextureManager::inst().bindTexture("terrain-heightmap.tga", GL_TEXTURE0_ARB);
	fx_wraping.setUniform1i("tex0", 0);

	m_fullscreenQuad.Draw();
}

void WarpingScene::handleKeyboardEvent(recore::Key key)
{
	switch (key) {
	default: return;
	}
}
