#include "pbatch.h"
#include "../shadermanager.h"

#define VERTEX_DATA     0
#define NORMAL_DATA     1
#define COLOR_DATA      2
#define TEXTURE_DATA    3
#define INDEX_DATA      4

using namespace render;

GLMyBatch::GLMyBatch()
	: vertexArrayBufferObject(0),
	  nNumIndexes(0), nNumVerts(0), bBatchDone(false),
	  pIndexes(NULL), pVerts(NULL), pNormals(NULL), pColors(NULL), pTexCoords(NULL)
{
}

GLMyBatch::~GLMyBatch()
{
	// Just in case these still are allocated when the object is destroyed
	delete [] pIndexes;
	delete [] pVerts;
	delete [] pNormals;
	delete [] pColors;
	delete [] pTexCoords;

	// Delete buffer objects
	glDeleteBuffers(5, bufferObjects);

	glDeleteVertexArrays(1, &vertexArrayBufferObject);
}

// Start the primitive batch.
void GLMyBatch::Begin(GLenum primitive, GLuint nIndexes, GLuint nVerts)
{
	// Just in case this gets called more than once...
	delete [] pIndexes;
	delete [] pVerts;
	delete [] pNormals;
	delete [] pColors;
	delete [] pTexCoords;

	primitiveType = primitive;
	nNumIndexes = nIndexes;
	nNumVerts = nVerts;

	// Allocate new blocks. In reality, the other arrays will be
	// much shorter than the index array
	pIndexes = new GLushort[nNumIndexes];
	pVerts = new M3DVector3f[nNumVerts];
	pNormals = new M3DVector3f[nNumVerts];
	pColors = new M3DVector4f[nNumVerts];
	pTexCoords = new M3DVector2f[nNumVerts];
}

// Block Copy in index data
void GLMyBatch::CopyIndexData(GLushort *vIndexes)
{
	// First time, create the buffer object, allocate the space
	if (bufferObjects[INDEX_DATA] == 0) {
		glGenBuffers(1, &bufferObjects[INDEX_DATA]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[INDEX_DATA]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * nNumIndexes, vIndexes, GL_STATIC_DRAW);
	}
	else	{ // Just bind to existing object
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[INDEX_DATA]);

		// Copy the data in
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(GLushort) * nNumIndexes, vIndexes);
		pIndexes = NULL;
	}
}

// Block Copy in vertex data
void GLMyBatch::CopyVertexData3f(M3DVector3f *vVerts)
{
	// First time, create the buffer object, allocate the space
	if (bufferObjects[VERTEX_DATA] == 0) {
		glGenBuffers(1, &bufferObjects[VERTEX_DATA]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[VERTEX_DATA]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * nNumVerts, vVerts, GL_STATIC_DRAW);
	}
	else	{ // Just bind to existing object
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[VERTEX_DATA]);

		// Copy the data in
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 3 * nNumVerts, vVerts);
		pVerts = NULL;
	}
}

// Block copy in normal data
void GLMyBatch::CopyNormalDataf(M3DVector3f *vNorms)
{
	// First time, create the buffer object, allocate the space
	if (bufferObjects[NORMAL_DATA] == 0) {
		glGenBuffers(1, &bufferObjects[NORMAL_DATA]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[NORMAL_DATA]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * nNumVerts, vNorms, GL_STATIC_DRAW);
	}
	else {	// Just bind to existing object
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[NORMAL_DATA]);

		// Copy the data in
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 3 * nNumVerts, vNorms);
		pNormals = NULL;
	}
}

void GLMyBatch::CopyColorData4f(M3DVector4f *vColors)
{
	// First time, create the buffer object, allocate the space
	if (bufferObjects[COLOR_DATA] == 0) {
		glGenBuffers(1, &bufferObjects[COLOR_DATA]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[COLOR_DATA]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 4 * nNumVerts, vColors, GL_STATIC_DRAW);
	}
	else {	// Just bind to existing object
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[COLOR_DATA]);

		// Copy the data in
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 4 * nNumVerts, vColors);
		pColors = NULL;
	}
}

void GLMyBatch::CopyTexCoordData2f(M3DVector2f *vTexCoords)
{
	// First time, create the buffer object, allocate the space
	if (bufferObjects[TEXTURE_DATA] == 0) {
		glGenBuffers(1, &bufferObjects[TEXTURE_DATA]);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[TEXTURE_DATA]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * nNumVerts, vTexCoords, GL_STATIC_DRAW);
	}
	else {	// Just bind to existing object
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[TEXTURE_DATA]);

		// Copy the data in
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 2 * nNumVerts, vTexCoords);
		pTexCoords = NULL;
	}
}

// Bind everything up in a little package
void GLMyBatch::End(void)
{
//	// Check to see if items have been added one at a time
//	if (pVerts != NULL) {
//		glBindBuffer(GL_ARRAY_BUFFER, uiVertexArray);
//		glUnmapBuffer(GL_ARRAY_BUFFER);
//		pVerts = NULL;
//	}

//	if (pColors != NULL) {
//		glBindBuffer(GL_ARRAY_BUFFER, uiColorArray);
//		glUnmapBuffer(GL_ARRAY_BUFFER);
//		pColors = NULL;
//	}

//	if (pNormals != NULL) {
//		glBindBuffer(GL_ARRAY_BUFFER, uiNormalArray);
//		glUnmapBuffer(GL_ARRAY_BUFFER);
//		pNormals = NULL;
//	}

//	for (unsigned int i = 0; i < nNumTextureUnits; i++)
//		if (pTexCoords[i] != NULL) {
//			glBindBuffer(GL_ARRAY_BUFFER, uiTextureCoordArray[i]);
//			glUnmapBuffer(GL_ARRAY_BUFFER);
//			pTexCoords[i] = NULL;
//		}

	// Create the master vertex array object
	glGenVertexArrays(1, &vertexArrayBufferObject);
	glBindVertexArray(vertexArrayBufferObject);

	if (bufferObjects[VERTEX_DATA] != 0) {
		glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[VERTEX_DATA]);
		glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}

	if (bufferObjects[NORMAL_DATA] != 0) {
		glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[NORMAL_DATA]);
		glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}

	if (bufferObjects[COLOR_DATA] != 0) {
		glEnableVertexAttribArray(GLT_ATTRIBUTE_COLOR);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[COLOR_DATA]);
		glVertexAttribPointer(GLT_ATTRIBUTE_COLOR, 4, GL_FLOAT, GL_FALSE, 0, 0);
	}

	if (bufferObjects[TEXTURE_DATA] != 0) {
		glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
		glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[TEXTURE_DATA]);
		glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	// Indexes
	if (bufferObjects[INDEX_DATA] != 0) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[INDEX_DATA]);
	}

	bBatchDone = true;
	glBindVertexArray(0);
}

// Just start over. No reallocations, etc.
void GLMyBatch::Reset(void)
{
	bBatchDone = false;
}

void GLMyBatch::Draw(void)
{
	if (!bBatchDone)
		return;

	glBindVertexArray(vertexArrayBufferObject);

	glDrawElements(primitiveType, nNumIndexes, GL_UNSIGNED_SHORT, 0);

	// Unbind to anybody
	glBindVertexArray(0);
}
