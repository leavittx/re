#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include "math/math3d.h"
#include "batchbase.h"

namespace render {

class GLMyBatch : public GLBatchBase
{
public:
    GLMyBatch();
	virtual ~GLMyBatch();

	// Start populating the array
	void Begin(GLenum primitive, GLuint nIndexes, GLuint nVerts);

	// Tell the batch you are done
	void End();

	// Block Copy in vertex data
	void CopyIndexData(GLushort *vIndexes);
	void CopyVertexData3f(M3DVector3f *vVerts);
	void CopyNormalDataf(M3DVector3f *vNorms);
	void CopyColorData4f(M3DVector4f *vColors);
	void CopyTexCoordData2f(M3DVector2f *vTexCoords);

	// Just to make life easier...
	inline void CopyVertexData3f(GLfloat *vVerts) { CopyVertexData3f((M3DVector3f *)(vVerts)); }
	inline void CopyNormalDataf(GLfloat *vNorms) { CopyNormalDataf((M3DVector3f *)(vNorms)); }
	inline void CopyColorData4f(GLfloat *vColors) { CopyColorData4f((M3DVector4f *)(vColors)); }
	inline void CopyTexCoordData2f(GLfloat *vTex) { CopyTexCoordData2f((M3DVector2f *)(vTex)); }

	virtual void Draw();

	void Reset();

protected:
	GLenum primitiveType;		// What am I drawing....

	GLuint bufferObjects[5];
	GLuint vertexArrayBufferObject;

	GLuint nNumIndexes;         // Number of indexes currently used
	GLuint nNumVerts;           // Number of vertices actually used

	bool bBatchDone;				// Batch has been built

	GLushort  *pIndexes;        // Array of indexes
	M3DVector3f *pVerts;        // Array of vertices
	M3DVector3f *pNormals;      // Array of normals
	M3DVector4f *pColors;       // Array of colors
	M3DVector2f *pTexCoords;   // Arrays of texture coordinates
};

} // end of namespace 'render'
