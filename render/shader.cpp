#include "shader.h"
#include "render/glhelper.h"

using namespace render;
using namespace std;

Shader::Shader()
{
}

Shader::~Shader()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Init and bind
//////////////////////////////////////////////////////////////////////////////////////////////////

void Shader::init(string& name, const char *vertexSource, const char *fragmentSource)
{
	m_name = name;
	m_hasBeenUsed = false;

	g_debug << "compiling shader " << m_name << endl;

	bool useDebug = true;

	if (useDebug) gl::debug("vertex1");
	m_vertex = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	if (useDebug) gl::debug("vertex2");
	glShaderSourceARB(m_vertex, 1, &vertexSource, NULL);
	if (useDebug) gl::debug("vertex3");
	glCompileShaderARB(m_vertex);
	if (useDebug) gl::debug("vertex4");
	debug(m_vertex, "output from vertex shader:");

	if (useDebug) gl::debug("fragment1");
	m_fragment = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	if (useDebug) gl::debug("fragment2");
	glShaderSourceARB(m_fragment, 1, &fragmentSource, NULL);
	if (useDebug) gl::debug("fragment3");
	glCompileShaderARB(m_fragment);
	if (useDebug) gl::debug("fragment4");
	debug(m_fragment, "output from fragment shader:");

	if (useDebug) gl::debug("program1");
	m_program = glCreateProgramObjectARB();
	if (useDebug) gl::debug("program2");
	glAttachObjectARB(m_program, m_vertex);
	if (useDebug) gl::debug("program3");
	glAttachObjectARB(m_program, m_fragment);
	if (useDebug) gl::debug("program4");
	glLinkProgramARB(m_program);
	if (useDebug) gl::debug("program5");
}

bool Shader::init(string& name, const char** vertexSource, const char** controlSource, const char** evaluationSource, const char** geometrySource, const char** fragmentSource)
{
	m_program = m_vertex = m_control = m_evaluation = m_geometry = m_fragment = 0;
	m_name = name;
	m_hasBeenUsed = false;

	g_debug << "compiling shader " << m_name << endl;

	if (!vertexSource || !fragmentSource)
	{
		g_debug << "ERROR: either vertex or fragment shader source is not specified" << endl;
		return false;
	}

	bool useDebug = true;
	GLint compiled;
	GLint logLength, charsWritten;
	char* log;

	m_vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(m_vertex, 1, (const char**) vertexSource, 0);
	glCompileShader(m_vertex);
	glGetShaderiv(m_vertex, GL_COMPILE_STATUS, &compiled);

	if (!compiled)
	{
		glGetShaderiv(m_vertex, GL_INFO_LOG_LENGTH, &logLength);
		log = (char*) malloc(logLength);
		glGetShaderInfoLog(m_vertex, logLength, &charsWritten, log);
		printf("Vertex shader compile error:\n");
		printf("%s\n", log);
		free(log);
		m_vertex = 0;
		return false;
	}

	if (*controlSource)
	{
		m_control = glCreateShader(GL_TESS_CONTROL_SHADER);
		glShaderSource(m_control, 1, (const char**) controlSource, 0);
		glCompileShader(m_control);
		glGetShaderiv(m_control, GL_COMPILE_STATUS, &compiled);

		if (!compiled)
		{
			glGetShaderiv(m_control, GL_INFO_LOG_LENGTH, &logLength);
			log = (char*) malloc(logLength);
			glGetShaderInfoLog(m_control, logLength, &charsWritten, log);
			printf("Control shader compile error:\n");
			printf("%s\n", log);
			free(log);
			m_control = 0;
			return false;
		}
	}

	if (*evaluationSource)
	{
		m_evaluation = glCreateShader(GL_TESS_EVALUATION_SHADER);
		glShaderSource(m_evaluation, 1, (const char**) evaluationSource, 0);
		glCompileShader(m_evaluation);
		glGetShaderiv(m_evaluation, GL_COMPILE_STATUS, &compiled);

		if (!compiled)
		{
			glGetShaderiv(m_evaluation, GL_INFO_LOG_LENGTH, &logLength);
			log = (char*) malloc(logLength);
			glGetShaderInfoLog(m_evaluation, logLength, &charsWritten, log);
			printf("Evaluation shader compile error:\n");
			printf("%s\n", log);
			free(log);
			m_evaluation = 0;
			return false;
		}
	}

	if (*geometrySource)
	{
		m_geometry = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(m_geometry, 1, (const char**) geometrySource, 0);
		glCompileShader(m_geometry);
		glGetShaderiv(m_geometry, GL_COMPILE_STATUS, &compiled);

		if (!compiled)
		{
			glGetShaderiv(m_geometry, GL_INFO_LOG_LENGTH, &logLength);
			log = (char*) malloc(logLength);
			glGetShaderInfoLog(m_geometry, logLength, &charsWritten, log);
			printf("Geometry shader compile error:\n");
			printf("%s\n", log);
			free(log);
			m_geometry = 0;
			return false;
		}
	}

	m_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_fragment, 1, (const char**) fragmentSource, 0);
	glCompileShader(m_fragment);
	glGetShaderiv(m_fragment, GL_COMPILE_STATUS, &compiled);

	if (!compiled)
	{
		glGetShaderiv(m_fragment, GL_INFO_LOG_LENGTH, &logLength);
		log = (char*) malloc(logLength);
		glGetShaderInfoLog(m_fragment, logLength, &charsWritten, log);
		printf("Fragment shader compile error:\n");
		printf("%s\n", log);
		free(log);
		m_fragment = 0;
		return false;
	}

	m_program = glCreateProgram();
	glAttachShader(m_program, m_vertex);

	if (m_control)
	{
		glAttachShader(m_program, m_control);
	}

	if (m_evaluation)
	{
		glAttachShader(m_program, m_evaluation);
	}

	if (m_geometry)
	{
		glAttachShader(m_program, m_geometry);
	}

	glAttachShader(m_program, m_fragment);

	// link the compiled shader program
	return link();
}

bool Shader::link()
{
	GLint linked;
	GLint logLength, charsWritten;
	char* log;

	glLinkProgram(m_program);
	glGetProgramiv(m_program, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logLength);
		log = (char*) malloc(logLength);
		glGetProgramInfoLog(m_program, logLength, &charsWritten, log);
		printf("Shader program link error:\n");
		printf("%s\n", log);
		free(log);
		m_program = 0;
		return false;
	}

	return true;
}

void Shader::release()
{
	if (m_program)
	{
		glDeleteProgram(m_program);
		m_program = 0;
	}
	if (m_fragment)
	{
		glDeleteShader(m_fragment);
		m_fragment = 0;
	}
	if (m_geometry)
	{
		glDeleteShader(m_geometry);
		m_geometry = 0;
	}
	if (m_evaluation)
	{
		glDeleteShader(m_evaluation);
		m_evaluation = 0;
	}
	if (m_control)
	{
		glDeleteShader(m_control);
		m_control = 0;
	}
	if (m_vertex)
	{
		glDeleteShader(m_vertex);
		m_vertex = 0;
	}
}

void Shader::bind()
{
	m_hasBeenUsed = true;
	glUseProgramObjectARB(m_program);
}

void Shader::debug(GLuint object, string type)
{
	if (glIsShader(object))
	{
		const int MAX_LOG_LENGTH = 4096;
		char log[MAX_LOG_LENGTH];
		int logLength = 0;
		glGetShaderInfoLog(object, MAX_LOG_LENGTH, &logLength, log);
		if (logLength > 0)
		{
			g_debug << type << endl;
			g_debug << log << endl;
		}
	}
	else
	{
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Parameter locations
//////////////////////////////////////////////////////////////////////////////////////////////////

GLint Shader::getUniformLocation(const GLcharARB *name)
{
	GLint location;
	location = glGetUniformLocationARB(m_program, name);
	if (location == -1)
	{
		g_debug << "Cannot find uniform variable " << name << " in shader " << m_name << endl;
	}
	return location;
}

GLint Shader::getAttribLocation(const GLcharARB *name)
{
	GLint location;
	location = glGetAttribLocationARB(m_program, name);
	if (location == -1)
	{
		g_debug << "Cannot find attrib " << name << " in shader " << m_name << endl;
	}
	return location;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Uniform values
//////////////////////////////////////////////////////////////////////////////////////////////////

bool Shader::setUniform1f(const char* name, GLfloat v0)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform1fARB(location, v0);
//	GL_UNIFORM_IS_ROW_MAJOR
	return true;
}

bool Shader::setUniform2f(const char* name, GLfloat v0, GLfloat v1)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform2fARB(location, v0, v1);

	return true;
}
bool Shader::setUniform3f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform3fARB(location, v0, v1, v2);
	return true;
}
bool Shader::setUniform4f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform4fARB(location, v0, v1, v2, v3);
	return true;
}

bool Shader::setUniform1i(const char* name, GLint v0)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform1i(location, v0);

	return true;
}
bool Shader::setUniform2i(const char* name, GLint v0, GLint v1)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform2i(location, v0, v1);
	return true;
}
bool Shader::setUniform3i(const char* name, GLint v0, GLint v1, GLint v2)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform3i(location, v0, v1, v2);
	return true;
}
bool Shader::setUniform4i(const char* name, GLint v0, GLint v1, GLint v2, GLint v3)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform4i(location, v0, v1, v2, v3);
	return true;
}

bool Shader::setUniform1fv(const char* name, GLsizei count, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform1fv(location, count, value);

	return true;
}
bool Shader::setUniform2fv(const char* name, GLsizei count, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;
	glUniform2fv(location, count, value);

	return true;
}
bool Shader::setUniform3fv(const char* name, GLsizei count, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;
	glUniform3fv(location, count, value);

	return true;
}
bool Shader::setUniform4fv(const char* name, GLsizei count, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform4fv(location, count, value);
	return true;
}
bool Shader::setUniform1iv(const char* name, GLsizei count, GLint *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform1iv(location, count, value);

	return true;
}
bool Shader::setUniform2iv(const char* name, GLsizei count, GLint *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform2iv(location, count, value);
	return true;
}
bool Shader::setUniform3iv(const char* name, GLsizei count, GLint *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform3iv(location, count, value);
	return true;
}
bool Shader::setUniform4iv(const char* name, GLsizei count, GLint *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniform4iv(location, count, value);
	return true;
}

bool Shader::setUniformMatrix2fv(const char* name, GLsizei count, GLboolean transpose, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniformMatrix2fv(location, count, transpose, value);
	return true;
}
bool Shader::setUniformMatrix3fv(const char* name, GLsizei count, GLboolean transpose, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniformMatrix3fv(location, count, transpose, value);
	return true;
}
bool Shader::setUniformMatrix4fv(const char* name, GLsizei count, GLboolean transpose, GLfloat *value)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glUniformMatrix4fv(location, count, transpose, value);
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Vertex attributes
//////////////////////////////////////////////////////////////////////////////////////////////////

bool Shader::setVertexAttrib1f(const char* name, GLfloat v0)
{
	int location = getAttribLocation(name);
	if (location == -1)
		return false;

	glVertexAttrib1f(location, v0);

	return true;
}
bool Shader::setVertexAttrib2f(const char* name, GLfloat v0, GLfloat v1)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;

	glVertexAttrib2f(location, v0, v1);
	return true;
}
bool Shader::setVertexAttrib3f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;
	glVertexAttrib3f(location, v0, v1, v2);

	return true;
}
bool Shader::setVertexAttrib4f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
	int location = getUniformLocation(name);
	if (location == -1)
		return false;
	glVertexAttrib4f(location, v0, v1, v2, v3);

	return true;
}
