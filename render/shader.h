#pragma once

#include "../globals.h"
#include <GL/glew.h>
#include <GL/gl.h>

namespace render {

class Shader
{
public:
	Shader();
	~Shader();

	// Init, release and bind
	void init(std::string& name, const char *vertexSource, const char *fragmentSource);
	bool init(std::string& name, const char** vertexSource, const char** controlSource, const char** evaluationSource, const char** geometrySource, const char** fragmentSource);
	bool link();

	void release();
	void bind();

	// Uniforms passing
	bool setUniform1f(const char* name, GLfloat v0);
	bool setUniform2f(const char* name, GLfloat v0, GLfloat v1);
	bool setUniform3f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2);
	bool setUniform4f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);

	bool setUniform1i(const char* name, GLint v0);
	bool setUniform2i(const char* name, GLint v0, GLint v1);
	bool setUniform3i(const char* name, GLint v0, GLint v1, GLint v2);
	bool setUniform4i(const char* name, GLint v0, GLint v1, GLint v2, GLint v3);

	bool setUniform1fv(const char* name, GLsizei count, GLfloat *value);
	bool setUniform2fv(const char* name, GLsizei count, GLfloat *value);
	bool setUniform3fv(const char* name, GLsizei count, GLfloat *value);
	bool setUniform4fv(const char* name, GLsizei count, GLfloat *value);
	bool setUniform1iv(const char* name, GLsizei count, GLint *value);
	bool setUniform2iv(const char* name, GLsizei count, GLint *value);
	bool setUniform3iv(const char* name, GLsizei count, GLint *value);
	bool setUniform4iv(const char* name, GLsizei count, GLint *value);

	bool setUniformMatrix2fv(const char* name, GLsizei count, GLboolean transpose, GLfloat *value);
	bool setUniformMatrix3fv(const char* name, GLsizei count, GLboolean transpose, GLfloat *value);
	bool setUniformMatrix4fv(const char* name, GLsizei count, GLboolean transpose, GLfloat *value);

	// Vertex attributes
	bool setVertexAttrib1f(const char* name, GLfloat v0);
	bool setVertexAttrib2f(const char* name, GLfloat v0, GLfloat v1);
	bool setVertexAttrib3f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2);
	bool setVertexAttrib4f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);

	GLhandleARB getVS() const { return m_vertex; }
	GLhandleARB getFS() const { return m_fragment; }
	GLhandleARB getProgram() const { return m_program; }
	bool hasBeenUsed() { return m_hasBeenUsed; }

private:
	GLint getAttribLocation(const GLcharARB *name);
	GLint getUniformLocation(const GLcharARB *name);
	void debug(GLuint object, std::string type);

	bool m_hasBeenUsed;
	std::string m_name;

	GLhandleARB m_program;

	GLhandleARB m_vertex;
	GLhandleARB m_control;
	GLhandleARB m_evaluation;
	GLhandleARB m_geometry;
	GLhandleARB m_fragment;
};

} // end of namespace 'render'
