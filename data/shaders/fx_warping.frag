//#version 330 compatibility

//varying vec2 texture_coordinate;
uniform vec2 resolution;
uniform float time;
uniform sampler2D tex0;

void main()
{
	// Sampling The Texture And Passing It To The Frame Buffer
//	gl_FragColor = texture2D(tex0, texture_coordinate);

	vec2 p = -2.0 + 1.0 * gl_FragCoord.xy / resolution.xy;
	vec3 col = texture2D(tex0, p/*gl_TexCoord[0].xy*/).xyz;

	gl_FragColor = vec4(col, 1.0f) * vec4(1.0f, 1.0f, 1.0f, 1.0f);
}


//uniform sampler2D qt_Texture0;
//varying vec4 qt_TexCoord0;

//void main(void)
//{
//    gl_FragColor = texture2D(qt_Texture0, qt_TexCoord0.st);
//}
