#version 410

//uniform sampler2D terrain;
uniform mat4 modelview;
uniform mat4 projection;
uniform vec2 screen_size;

in vec4 position;
in vec3 normal;
out _{
	vec4 world;
	vec3 eye;
	float dist;
	vec3 eye_normal;
	vec4 device;
	vec2 screen;
	bool offscreen;
	vec3 normal;
} o;

void main()
{
	vec2 texcoord = position.xy;
	o.world = position;
	o.eye = (modelview * o.world).xyz;
	o.dist = length(o.eye);
	o.eye_normal = normalize(o.eye);
	o.device = projection * modelview * o.world;
	o.device = clamp(o.device/o.device.w, -1.6, 1.6);
	o.screen = (o.device.xy+1) * (screen_size*0.5);
	o.normal = normal;

	if (o.device.z < -0.5)
	{
		o.offscreen = true;
	}
	else
	{
		o.offscreen = any(lessThan(o.device.xy, vec2(-1.59)) ||
						  greaterThan(o.device.xy, vec2(1.59)));
	}
}
